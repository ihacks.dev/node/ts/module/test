// Imports
import typescript from "@rollup/plugin-typescript";
import { uglify } from "rollup-plugin-uglify";

export default {
	input: "./src/index.ts",
	output: {
		dir: "./",
		format: "cjs",
		sourcemap: true
	},
	plugins: [
		typescript({ tsconfig: "./tsconfig.json" }),
		uglify()
	]
};
