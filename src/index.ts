// Exports

/**
 * This is the default emoticon.
 */
export const defaultEmoji: string = "👋";

/**
 * Say something to someone.
 * @param word The word to say to a person.
 * @param to The person to say something to.
 * @param emoji The emoji to append after the text.
 * @returns The comined data.
 */
export const say = (word: string, to: string, emoji: string): string => `${word} ${to} ${emoji}`;

/**
 * Says hi to person.
 * @param to The name of the person to say hi to.
 * @param emoji The emohi to put after the name.
 * @returns A string that contains the greeting.
 */
export const sayHi = (to: string, emoji: string = defaultEmoji): string => say("Hi", to, emoji);

/**
 * Greet a user farewell.
 * @param to The person to say goodbye to.
 * @param emoji The emohi to put after the name.
 * @returns A string that contains the farewell mesage.
 */
export const sayBye = (to: string, emoji: string = defaultEmoji): string => say("Bye", to, emoji);
