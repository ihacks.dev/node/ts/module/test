# @ihacks.dev/test

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/d7b0ba389b924d119843ea705357efc9)](https://www.codacy.com/gl/ihacks.dev/test) &nbsp;
[![pipeline status](https://gitlab.com/ihacks.dev/node/ts/module/test/badges/master/pipeline.svg)](https://gitlab.com/ihacks.dev/node/ts/module/test/-/commits/master) &nbsp;
[![coverage report](https://gitlab.com/ihacks.dev/node/ts/module/test/badges/master/coverage.svg)](https://gitlab.com/ihacks.dev/node/ts/module/test/-/commits/master)

> A test project to test the setup with CI/CD and GitLab.

## Installation

```bash
yarn add @ihacks.dev/test
# or
npm i -S @ihacks.dev/test
```

## Usage

<!-- :documentation -->

## License

See [LICENSE](./LICENSE).
