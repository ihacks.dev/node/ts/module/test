// Imports
const {
	say,
	sayBye,
	sayHi
} = require("../");

// Tests
it("Say ✉️ to 👱‍", () => expect(say("You are cool", "John", "😎")).toStrictEqual("You are cool John 😎"));
it("Says hi 👋", () => ( expect(sayHi("John")).toStrictEqual("Hi John 👋"), expect(sayHi("John", "🙋‍♂️")).toStrictEqual("Hi John 🙋‍♂️") ));
it("Says bye 👋", () => ( expect(sayBye("John")).toStrictEqual("Bye John 👋"), expect(sayBye("John", "🙋‍♂️")).toStrictEqual("Bye John 🙋‍♂️") ));
